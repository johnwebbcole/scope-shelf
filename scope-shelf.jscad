// title      : scopeShelf
// author     : John Cole
// license    : ISC License
// file       : scopeShelf.jscad

/* exported main, getParameterDefinitions */

function getParameterDefinitions() {
  return [
    {
      name: 'tripod_diameter',
      caption: 'Diameter (inch):',
      type: 'float',
      initial: 1.9
    },
    {
      name: 'tripod_angle',
      caption: 'Angle (degrees):',
      type: 'float',
      initial: 23
    },
    {
      name: 'resolution',
      type: 'choice',
      values: [0, 1, 2, 3, 4],
      captions: [
        'very low (6,16)',
        'low (8,24)',
        'normal (12,32)',
        'high (24,64)',
        'very high (48,128)'
      ],
      initial: 3,
      caption: 'Resolution:'
    }
  ];
}

function main(params) {
  var resolutions = [[6, 16], [8, 24], [12, 32], [32, 64], [64, 128]];
  CSG.defaultResolution3D = resolutions[params.resolution][0];
  CSG.defaultResolution2D = resolutions[params.resolution][1];
  util.init(CSG);

  var tripod_diameter = util.inch(params.tripod_diameter);
  var tripod_angle = -params.tripod_angle;

  var clip = Clip(tripod_diameter, 95)
    .rotate('clip', 'z', 45)
    .rotate('clip', 'x', tripod_angle)
    .translate([0, 0, -15]);

  var tabletbase_shape = PiTabletBase();
  var tabletbase_half = util
    .poly2solid(tabletbase_shape, tabletbase_shape, 5)
    .fit([130, 43, 0])
    .bisect('x')
    .parts.negative.rotateX(180);

  var tabletbase = union([
    tabletbase_half,
    tabletbase_half
      .rotateZ(180)
      .rotateX(180)
      .snap(tabletbase_half, 'x', 'outside-')
      .snap(tabletbase_half, 'zy', 'inside-')
      .translate([-0.00001, 0, 0])
  ])
    .Zero()
    .snap(clip.combine('clip'), 'y', 'outside+')
    .align(clip.combine('clip'), 'x')
    .color('blue');

  var frontlip_shape = PiTabletFrontLip();
  var frontlip = util
    .poly2solid(frontlip_shape, frontlip_shape, 10)
    .fit([130, 0, 0])
    .rotateZ(180)
    .snap(tabletbase, 'y', 'outside+')
    .align(tabletbase, 'x')
    .fillet(1, 'z+')
    .color('darkblue');

  var rib = Parts.Cube([20, 5, 10])
    .Center()
    .chamfer(-5, 'z+')
    .intersect(Parts.Cube([30, 5, 10]).Center())
    .rotateX(90)
    .Zero()
    .align(tabletbase, 'x')
    .snap(tabletbase, 'y', 'outside-')
    .color('lightblue');

  var outertop = Parts.Cube([200, 200, 65])
    .align(clip.combine('clip'), 'xy')
    .Zero()
    .color('red');

  var anchor = Anchor().align('hole', tabletbase, 'xyz');
  var hook = UBend(5, 5)
    // .rotate('inlet', 'x', 90)
    // .rotate('inlet', 'z', -90)
    .rotate('bend', 'y', 90)
    .align('inlet', clip.combine('clip'), 'xy')
    .snap('inlet', clip.combine('clip'), 'z', 'inside+')
    .translate([0, -12, -25]);

  return union([
    // util.unitAxis(10),
    tabletbase.subtract(anchor.combine('hole')).union(anchor.combine('post')),
    clip.combine('clip'),
    rib.subtract(clip.combine('pipe')),
    frontlip,
    hook
      .combine()
      .color('darkblue')
      .subtract(clip.combine('pipe'))
  ])
    .intersect(outertop)
    .Center();
}

function UBend(
  diameter = 15,
  outer_radius = 20,
  in_lenght = 15,
  out_lenght = 15
) {
  var bend = torus({
    ri: diameter / 2,
    ro: outer_radius,
    fni: CSG.defaultResolution2D,
    fno: CSG.defaultResolution3D
  })
    .Zero()
    .subtract(
      Parts.Cube([
        outer_radius * 4,
        outer_radius * 2,
        outer_radius * 2
      ]).translate([-outer_radius * 2, 0, 0])
    );

  var inlet = Parts.Cylinder(diameter, in_lenght, {
    resolution: CSG.defaultResolution2D
  })
    .rotateX(90)
    .align(bend, 'z')
    .snap(bend, 'x', 'inside-')
    .snap(bend, 'y', 'outside-')
    .color('blue');

  var outlet = Parts.Cylinder(diameter, out_lenght, {
    resolution: CSG.defaultResolution2D
  })
    .rotateX(90)
    .align(bend, 'z')
    .snap(bend, 'x', 'inside+')
    .snap(bend, 'y', 'outside-')
    .color('red');

  return util.group({ bend, inlet, outlet });
}

function Elbow(
  diameter = 15,
  outer_radius = 20,
  in_lenght = 15,
  out_lenght = 15
) {
  var elbow = torus({
    ri: diameter / 2,
    ro: outer_radius,
    fni: CSG.defaultResolution2D,
    fno: CSG.defaultResolution3D
  })
    .Zero()
    .intersect(
      Parts.Cube([outer_radius * 2, outer_radius * 2, outer_radius * 2])
    );

  var inlet = Parts.Cylinder(diameter, in_lenght, {
    resolution: CSG.defaultResolution2D
  })
    .rotateY(90)
    .rotateX(90)
    .align(elbow, 'z')
    .snap(elbow, 'x', 'outside+')
    .snap(elbow, 'y', 'inside+');

  var outlet = Parts.Cylinder(diameter, out_lenght, {
    resolution: CSG.defaultResolution2D
  })
    .rotateX(90)
    .align(elbow, 'z')
    .snap(elbow, 'x', 'inside+')
    .snap(elbow, 'y', 'outside+');
  return util.group({ elbow, inlet, outlet });
}

function Anchor(width = 10, height = 10) {
  var hole = Parts.Cylinder(width, height, {
    resolution: CSG.defaultResolution2D
  })
    .Center()
    .color('red');
  var post = Parts.Cylinder(height / 2, width * 0.66, {
    resolution: CSG.defaultResolution2D
  })
    .rotateX(90)
    .align(hole, 'xz')
    .snap(hole, 'y', 'inside-')
    .translate([0, 0, -height / 6])
    .color('purple');
  return util.group({ post, hole });
}
function Clip(OD, height = 25) {
  // console.log("Nozzel", util);
  var pipe = Parts.Cylinder(OD, height, {
    resolution: CSG.defaultResolution2D
  }).color('yellow');

  var thickness = util.nearest.over(5, 0.35);

  // console.log('Clip thickness', thickness);
  var body = Parts.Cylinder(OD + thickness, height, {
    resolution: CSG.defaultResolution2D
  })
    .color('blue')
    .subtract([pipe]);

  var cube = Parts.Cube([height, height, height])
    .translate([thickness / 2, thickness / 2, 0])
    .color('red');

  return util.group('clip,pipe', {
    clip: union([body, Parts.Cube([height, height, height]).color('blue')])
      .subtract([cube])
      .subtract(pipe)
      .intersect(
        Parts.Cylinder(OD * 1.25, height, {
          resolution: CSG.defaultResolution2D
        }).color('blue')
      ),
    pipe,
    cube
  });
}

// ********************************************************
// Other jscad libraries are injected here.  Do not remove.
// Install jscad libraries using NPM
// ********************************************************
// include:js
// endinject
